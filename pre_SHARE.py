import httplib2
from bs4 import BeautifulSoup
import time
import numpy as np
import gdal

def READ_GEOTIFF(fname, xyRange=None, ravel=False):

    f = gdal.Open(fname)
    dataRGB = f.ReadAsArray()
    pixelNx = f.RasterXSize
    pixelNy = f.RasterYSize
    geoInfo = f.GetGeoTransform()

    x0 = np.arange(pixelNx)
    y0 = np.arange(pixelNy)
    yy0, xx0 = np.meshgrid(y0, x0)

    # if in normal projection, xx, yy represent longitude and latitude
    # if in  polar projection, a further step is needed to get longitude and latitude
    xx  = geoInfo[0] + geoInfo[1]*xx0 + geoInfo[2]*yy0
    yy  = geoInfo[3] + geoInfo[4]*xx0 + geoInfo[5]*yy0
    RGB = np.transpose(dataRGB)

    Nx = ((xx[:, 0]>=xyRange[0])&(xx[:, 0]<=xyRange[2])).sum()
    Ny = ((yy[0, :]>=xyRange[1])&(yy[0, :]<=xyRange[3])).sum()

    xx  = xx.ravel()
    yy  = yy.ravel()
    RGB = RGB.reshape((-1, 3))

    if xyRange is not None:
        logic = (xx>=xyRange[0])&(xx<=xyRange[2]) & (yy>=xyRange[1])&(yy<=xyRange[3])
        if not ravel:
            return (xx[logic]).reshape((Nx, Ny)), (yy[logic]).reshape((Nx, Ny)), (RGB[logic, ...]/255.0).reshape((Nx, Ny, 3))
        else:
            return xx[logic], yy[logic], RGB[logic, ...]/255.0
    else:
        if not ravel:
            return xx.reshape((pixelNx, pixelNy)), yy.reshape((pixelNx, pixelNy)), (RGB/255.0).reshape((pixelNx, pixelNy, 3))
        else:
            return xx, yy, RGB/255.0

def GLINK_NAMES(webDir, searchTags=['']):

    http = httplib2.Http()
    status, response = http.request(webDir, 'GET')

    soup =  BeautifulSoup(response, "lxml")

    fileNames = []
    for link in soup.find_all(['a']):
        if not link.find('img'):
            fileName = link['href']
            if all(searchTag in fileName for searchTag in searchTags):
                fileNames.append(fileName)

    fileNames = sorted(fileNames)
    return fileNames

if __name__ == '__main__':
    webDir = 'https://ladsweb.modaps.eosdis.nasa.gov/opendap'
    GLINK_NAMES(webDir)
