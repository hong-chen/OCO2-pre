#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import glob
import datetime
import multiprocessing as mp
import h5py
import numpy as np
from scipy import interpolate
from scipy.io import readsav
from pre_SHARE import GLINK_NAMES
from cfg import *
import netCDF4 as nc4

fname_netrc = os.path.expanduser('~/.netrc')
if not os.path.isfile(fname_netrc):
    os.system('touch %s' % fname_netrc)
    os.system('echo "machine urs.earthdata.nasa.gov login %s password %s" >> %s' % (cfg['username'], cfg['password'], fname_netrc))

fname_urs   = os.path.expanduser('~/.urs_cookies')
if not os.path.isfile(fname_urs):
    os.system('touch %s' % fname_urs)

def GDATA_OCO2(
        date,
        fdirOut=os.getcwd(),
        httpSite='https://oco2.gesdisc.eosdis.nasa.gov/data/s4pa/OCO2_DATA/OCO2_L2_Lite_FP.7r',
        verbose=True
        ):

    linkFull  = '%s/%4.4d' % (httpSite, date.year)
    dateTag   = '%2.2d%2.2d%2.2d' % (date.year-2000, date.month, date.day)
    searchTags = ['oco2', dateTag, 'nc4']
    fileNames = GLINK_NAMES(linkFull, searchTags=searchTags)
    NFile = len(fileNames)
    if NFile > 0:
        print('Message [GDATA_OCO2]: start download for OCO2 ...')
    else:
        exit('Error   [GDATA_OCO2]: data is not available for the requested date.')

    for fileName in fileNames:
        dataLink = '%s/%s' % (linkFull, fileName)
        fname0 = '%s/%s' % (fdirOut, fileName)
        if glob.glob(fname0):
            print('Warning [GDATA_OCO2]: %s already exists under %s, skipping...' % (fileName, fdirOut))
        else:
            cmdStr = 'wget --load-cookies ~/.urs_cookies --save-cookies ~/.urs_cookies --keep-session-cookies -P %s/ -q %s' % (fdirOut, dataLink)
            os.system(cmdStr)
            if verbose and glob.glob(fname0):
                print('Message [GDATA_OCO2]: %s has been downloaded under %s.' %(fileName, fdirOut))

    NFile_check = len(glob.glob('%s/*' % fdirOut))
    if NFile_check == NFile:
        print('Message [GDATA_OCO2]: complete download for OCO2 ...')
    else:
        print('Warning [GDATA_OCO2]: incomplete download for OCO2, please check ...')

class RDATA_OCO2:
    def __init__(self, fname, xyRange=None):
        f = nc4.Dataset(fname, 'r')
        self.lon = f.variables['longitude'][...].ravel()
        self.lat = f.variables['latitude'][...].ravel()
        self.xco2= f.variables['xco2'][...].ravel()
        f.close()

        if xyRange is not None:
            self.UPDATE(xyRange)

    def UPDATE(self, xyRange):
        logic = (self.lon>=xyRange[0])&(self.lon<=xyRange[2]) & (self.lat>=xyRange[1])&(self.lat<=xyRange[3])
        self.lon = self.lon[logic]
        self.lat = self.lat[logic]
        self.xco2= self.xco2[logic]

if __name__ == "__main__":
    # date = datetime.datetime(2017, 3, 5)
    # GDATA_OCO2(date)
    fname  = '/Users/hoch4240/Chen/work/03_OCO2/pre/data/20151206/OCO2/oco2_LtCO2_151206_B7305Br_160712000329s.nc4'
    f_oco2 = RDATA_OCO2(fname)
    print(f_oco2.lon)
