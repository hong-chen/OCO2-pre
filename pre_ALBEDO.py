#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import glob
import datetime
import multiprocessing as mp
import h5py
import numpy as np
from scipy import interpolate
from scipy.io import readsav
from pre_SHARE import GLINK_NAMES
from pyhdf.SD import SD, SDC
from cfg import *

fname_netrc = os.path.expanduser('~/.netrc')
if not os.path.isfile(fname_netrc):
    os.system('touch %s' % fname_netrc)
    os.system('echo "machine urs.earthdata.nasa.gov login %s password %s" >> %s' % (cfg['username'], cfg['password'], fname_netrc))

fname_urs   = os.path.expanduser('~/.urs_cookies')
if not os.path.isfile(fname_urs):
    os.system('touch %s' % fname_urs)

def GDATA_ALBEDO_V1(
        date,
        fdirOut,
        httpSite='https://e4ftl01.cr.usgs.gov/MOTA/MCD43C3.005',
        verbose=True
        ):

    linkFull = httpSite
    searchTags = ['.', '/']
    fileNames = GLINK_NAMES(linkFull, searchTags=searchTags)
    NFile     = len(fileNames)

    dtime0    = datetime.datetime(1999, 12, 31)
    jdays     = np.repeat(np.nan, NFile)
    for i in range(NFile):
        fileName = fileNames[i]
        dateStr  = fileName.replace('/', '')
        try:
            dtime    = datetime.datetime.strptime(dateStr, '%Y.%m.%d')
            jdays[i] = (dtime-dtime0).total_seconds()/86400.0
        except ValueError:
            pass

    jday0 = (date-dtime0).total_seconds()/86400.0
    index = np.nanargmin(np.abs(jday0-jdays))

    days_diff = np.abs(jdays[index]-jday0)
    if days_diff > 0:
        date_pick = fileNames[index].replace('/', '').replace('.', '-')
        date_ori  = date.strftime('%Y-%m-%d')
        print('Warning [GDATA_ALBEDO_V1]: picked %s for date %s with %d-day difference.' % (date_pick, date_ori, days_diff))

    linkFull = '%s/%s' % (linkFull, fileNames[index])
    searchTags = ['MCD', 'hdf']
    fileNames = GLINK_NAMES(linkFull, searchTags=searchTags)
    NFile = len(fileNames)
    if NFile > 0:
        print('Message [GDATA_ALBEDO_V1]: start download for MCD albedo ...')
    else:
        exit('Error   [GDATA_ALBEDO_V1]: data is not available for the requested date.')

    for fileName in fileNames:
        dataLink = '%s/%s' % (linkFull, fileName)
        fname0 = '%s/%s' % (fdirOut, fileName)
        if glob.glob(fname0):
            print('Warning [GDATA_ALBEDO_V1]: %s already exists under %s, skipping...' % (fileName, fdirOut))
        else:
            cmdStr = 'wget --load-cookies ~/.urs_cookies --save-cookies ~/.urs_cookies --keep-session-cookies -P %s/ -q %s' % (fdirOut, dataLink)
            os.system(cmdStr)
            if verbose and glob.glob(fname0):
                print('Message [GDATA_ALBEDO_V1]: %s has been downloaded under %s.' %(fileName, fdirOut))

    NFile_check = len(glob.glob('%s/*' % fdirOut))
    if NFile_check == NFile:
        print('Message [GDATA_ALBEDO_V1]: complete download for MCD albedo ...')
    else:
        print('Warning [GDATA_ALBEDO_V1]: incomplete download for MCD albedo, please check ...')

class RDATA_ALBEDO_V1:

    def __init__(self, fname, xyRange=None):
        f = SD(fname, SDC.READ)
        for vname in f.datasets().keys():
            d = f.select(vname)
            print(vname)
            print(d.attributes())
            print()
        f.end()

        if xyRange is not None:
            self.UPDATE(xyRange)

    def UPDATE(self, xyRange):
        logic = (self.lon>=xyRange[0])&(self.lon<=xyRange[2]) & (self.lat>=xyRange[1])&(self.lat<=xyRange[3])
        self.lon = self.lon[logic]
        self.lat = self.lat[logic]
        self.xco2= self.xco2[logic]

if __name__ == "__main__":
    # date = datetime.datetime(2017, 3, 5)
    # GDATA_ALBEDO_V1(date, os.getcwd())
    fname = '/Users/hoch4240/Chen/work/03_OCO2/pre/data/20151206/MCD/MCD43C3.A2015337.005.2015365205333.hdf'
    RDATA_ALBEDO_V1(fname)
