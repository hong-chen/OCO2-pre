#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import glob
import datetime
import multiprocessing as mp
import h5py
import numpy as np
from scipy import interpolate
from scipy.io import readsav
import httplib2
from bs4 import BeautifulSoup
from cfg import *
from pyhdf.SD import SD, SDC

fname_netrc = os.path.expanduser('~/.netrc')
if not os.path.isfile(fname_netrc):
    os.system('touch %s' % fname_netrc)
    os.system('echo "machine urs.earthdata.nasa.gov login %s password %s" >> %s' % (cfg['username'], cfg['password'], fname_netrc))

fname_urs   = os.path.expanduser('~/.urs_cookies')
if not os.path.isfile(fname_urs):
    os.system('touch %s' % fname_urs)

def GDATA_CALIPSO(
        date,
        fdir_out,
        httpSite='https://oco2.gesdisc.eosdis.nasa.gov/data/s4pa/OCO2_DATA/OCO2_L2_Lite_FP.7r',
        verbose=True
        ):

    linkFull  = '%s/%4.4d' % (httpSite, date.year)
    dateTag   = '%2.2d%2.2d%2.2d' % (date.year-2000, date.month, date.day)
    searchTags = ['oco2', dateTag, 'nc4']
    fileNames = WEB_DATA_FILENAMES(linkFull, searchTags)

    for fileName in fileNames:
        dataLink = '%s/%s' % (linkFull, fileName)
        fname0 = '%s/%s' % (fdir_out, fileName)
        if glob.glob(fname0):
            print('Warning [GDATA_OCO2]: %s already exists under %s, skipping...' % (fileName, fdir_out))
        else:
            cmdStr = 'wget --load-cookies ~/.urs_cookies --save-cookies ~/.urs_cookies --keep-session-cookies -P %s/ -q %s' % (fdir_out, dataLink)
            os.system(cmdStr)
            if verbose and glob.glob(fname0):
                print('Message [GDATA_OCO2]: %s has been downloaded under %s.' %(fileName, fdir_out))

def READ_CALIPSO_CLD(fname):
    f = SD(fname, SDC.READ)
    vnames = f.datasets().keys()
    # cld_opt = f.select('Column_Optical_Depth_Cloud_532')[:]
    # aer_opt = f.select('Column_Optical_Depth_Aerosols_532')[:]
    # lon     = f.select('Longitude')[:][:, 0]   # index 0-average 1-mid 2-last
    # lat     = f.select('Latitude')[:][:, 0]
    for vname in vnames:
        print(f.select(vname)[:].shape, vname)
    f.end()

def READ_CALIPSO(fname):
    f = SD(fname, SDC.READ)
    lon = f.select('Longitude')[:]
    lat = f.select('Latitude')[:]
    data_obj = f.select('Feature_Classification_Flags')
    data     = data_obj[:]
    aer_type = np.zeros(data.shape, dtype=np.int32)
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            bin0 = np.binary_repr(data[i, j], width=16)
            val1 = int(bin0[:3], 2)
            val2 = int(bin0[3:5], 2)
            val3 = int(bin0[5:7], 2)
            val4 = int(bin0[7:9], 2)
            val5 = int(bin0[9:12], 2)
            val6 = int(bin0[12], 2)
            val7 = int(bin0[13:], 2)
            if val1 == 3:
                aer_type[i, j] = val5
            elif val1 == 0:
                aer_type[i, j] = -2
            else:
                aer_type[i, j] = -1

    # cld_opt = f.select('Column_Optical_Depth_Cloud_532')[:]
    # aer_opt = f.select('Column_Optical_Depth_Aerosols_532')[:]
    # lon     = f.select('Longitude')[:][:, 0]   # index 0-average 1-mid 2-last
    # lat     = f.select('Latitude')[:][:, 0]
    # for vname in vnames:
    #     print(f.select(vname)[:].shape, vname)
    f.end()

    import h5py
    f = h5py.File('forYXS.h5', 'w')
    f['aer_type'] = aer_type
    f['lon']      = lon
    f['lat']      = lat
    f.close()

def ROW2BLOCK(row):

    blk_len1 = 55
    blk_num1 = 3
    blk_len2 = 200
    blk_num2 = 5
    blk_len3 = 290
    blk_num3 = 15

    index0 = 0
    blk1 = np.array([], dtype=np.int32)
    for i in range(blk_num1):
        index_s = int(index0  + i*blk_len1)
        index_e = int(index_s + blk_len1)
        blk1 = np.append(blk1, np.tile(row[index_s:index_e], int(blk_num3/blk_num1)))
    blk1 = blk1.reshape((blk_num3, -1))

    index0 = index_e
    blk2 = np.array([], dtype=np.int32)
    for i in range(blk_num2):
        index_s = int(index0  + i*blk_len2)
        index_e = int(index_s + blk_len2)
        blk2 = np.append(blk2, np.tile(row[index_s:index_e], int(blk_num3/blk_num2)))
    blk2 = blk2.reshape((blk_num3, -1))

    index0 = index_e
    blk3 = row[index0:].reshape((blk_num3, -1))

    blk  = np.hstack((np.hstack((blk1, blk2)), blk3))

    return blk[:, ::-1]

if __name__ == "__main__":
    # fname = '/Users/hoch4240/Downloads/CAL_LID_L2_VFM-ValStage1-V3-01.2007-07-23T10-10-15ZN.hdf'
    # READ_CALIPSO(fname)
    # exit()
    # indices = np.arange(15*3744)
    # exit()

    import h5py
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    from matplotlib.ticker import FixedLocator
    from mpl_toolkits.basemap import Basemap

    f = h5py.File('forYXS.h5', 'r')
    aer_type = f['aer_type'][...]
    lon      = f['lat'][...][:, 0]
    f.close()

    blk_all = np.zeros((15*aer_type.shape[0], 545), dtype=np.int32)
    for i in range(aer_type.shape[0]):
        row = aer_type[i, :]
        blk = ROW2BLOCK(row)
        index_s = int(15*i)
        index_e = int(index_s+15)
        blk_all[index_s:index_e, :] = blk

    aer_type = blk_all.copy()
    indices  = np.arange(7, aer_type.shape[0], 15)

    # figure settings
    fig = plt.figure(figsize=(8, 6))
    ax1 = fig.add_subplot(111)
    levels = np.arange(8)
    x = np.arange(aer_type.shape[0])
    y = np.arange(aer_type.shape[1])
    YY, XX = np.meshgrid(y, x)
    cs1 = ax1.contourf(XX, YY, aer_type, levels=levels, cmap='jet')

    ax1.xaxis.set_major_locator(FixedLocator(x[indices][::800]))
    print(lon[::800])
    ax1.set_xticklabels(lon[::800])
    # XX, YY = np.meshgrid(x, y)
    # cs1 = ax1.contourf(XX, YY, np.transpose(aer_type), levels=levels, cmap='jet')

    plt.colorbar(cs1)
    # plt.legend(loc='best', fontsize=12, framealpha=0.4)
    plt.savefig('test.png')
    plt.show()

    exit()

    # fname = '/Users/hoch4240/Chen/work/03_OCO2/pre/data/20151206/CALIPSO/CAL_LID_L2_05kmCLay-Prov-V3-30.2015-12-06T01-31-24ZN.hdf'
    fname = '/Users/hoch4240/Downloads/CAL_LID_L2_VFM-ValStage1-V3-01.2007-07-23T10-10-15ZN.hdf'
    READ_CALIPSO(fname)
    exit()


    import re
    from robobrowser import RoboBrowser

    base_url = 'https://www-calipso.larc.nasa.gov/search/login.php'
    browser = RoboBrowser(history=True)
    browser.open(base_url)
    form = browser.get_form(action='Login')

    form["username"] = 'hoch4240'
    form["password"] = '3142578Syx#'
    browser.session.headers['Referer'] = base_url

    browser.submit_form(form)
    print(str(browser.select))
    # date = datetime.datetime(2017, 3, 5)
    # GDATA_OCO2(date, '.')
