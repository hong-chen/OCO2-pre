## pre_MODIS

Tested on macOS v10.12.3 with

- Python v3.6.0 :: Anaconda 4.3.1 (x86_64)
  - os
  - sys
  - glob
  - ftplib
  - datetime
  - [PyDAP](http://www.pydap.org/en/latest/)

--------

### Description

Full function: `GDATA_MODIS(date, tag='allData/6/MOD06_L2', fdirOut='.')`

- `date`

  input date, specified by `datetime.datetime` module.

- `tag='allData/6/MOD06_L2'`

  default directory for Terra MODIS cloud L2 data on FTP data archive.

  For example, Aqua MODIS cloud L2 data would be `tag='allData/6/MYD03'`.

- `fdirOut='.'`

  use current directory as default local directory to store downloaded data.

  This can be set to any existing directory, e.g., `fdirOut='/data/MODIS/Terra/20040101'`.

### __How to use__

For example, download Terra MODIS cloud L2 product for 2004-01-01.

- Add executable permission

  `chmod +x pre_MODIS`

- Run `pre_MODIS`

  - Interactive mode

    `./pre_MODIS`

    then type in the date

    `2004-01-01`

  - with input date

    `./pre_MODIS 2004-01-01`
