import os
import numpy as np
import datetime
from pre_MODIS import GDATA_MODIS
from pre_OCO2 import GDATA_OCO2, RDATA_OCO2
from pre_ALBEDO import GDATA_ALBEDO_V1

class MAIN_INIT:

    def __init__(self,
            date,
            fdir_data = None
            ):

        dateStr = date.strftime('%Y%m%d')

        fdir_cur = os.getcwd()
        if fdir_data is None:
            fdir_data = '%s/data' % fdir_cur

        fdir_data_MODIS   = '%s/%s/MODIS'  % (fdir_data, dateStr)
        # download Terra MODIS
        GDATA_MODIS(date, 'allData/6/MOD06_L2', fdirOut=fdir_data_MODIS, verbose=True)
        GDATA_MODIS(date, 'allData/6/MOD03'   , fdirOut=fdir_data_MODIS, verbose=True)
        # download Aqua MODIS
        GDATA_MODIS(date, 'allData/6/MYD06_L2', fdirOut=fdir_data_MODIS, verbose=True)
        GDATA_MODIS(date, 'allData/6/MYD03'   , fdirOut=fdir_data_MODIS, verbose=True)
        exit()

        # download OCO2
        fdir_data_OCO2    = '%s/%s/OCO2'   % (fdir_data, dateStr)
        GDATA_OCO2(date, fdirOut=fdir_data_OCO2)

        # download MCD albedo
        fdir_data_MCD    = '%s/%s/MCD'   % (fdir_data, dateStr)
        GDATA_ALBEDO_V1(date, fdirOut=fdir_data_MCD)

        # fdir_data_CALIPSO = '%s/%s/CALIPSO'% (fdir_data, dateStr)

def line_select_callback(eclick, erelease):

    # fname = '/Users/hoch4240/Chen/work/03_OCO2/pre/data/20151206/nasa-worldview-2015-12-06.tiff'
    # xx, yy, RGB = READ_GEOTIFF(fname, xyRange=[144.0, -30.0, 146.0, -24.0], ravel=False)

    # xx0 = xx[:, 0]
    # yy0 = yy[0, :]
    # x_s = xx0[np.max([int(eclick.xdata), 0])]
    # y_s = yy0[np.max([int(eclick.ydata), 0])]
    # x_e = xx0[np.min([int(erelease.xdata)+1, xx0.size-1])]
    # y_e = yy0[np.min([int(erelease.ydata)+1, yy0.size-1])]

    # xmin = np.min([x_s, x_e])
    # ymin = np.min([y_s, y_e])
    # xmax = np.max([x_s, x_e])
    # ymax = np.max([y_s, y_e])

    xmin = np.min([eclick.xdata, erelease.xdata])
    ymin = np.min([eclick.ydata, erelease.ydata])
    xmax = np.max([eclick.xdata, erelease.xdata])
    ymax = np.max([eclick.ydata, erelease.ydata])

    xyRange= [xmin, ymin, xmax, ymax]

    print("Lower Left: (%.4f, %.4f) Upper Right:(%.4f, %.4f)" % (xyRange[0], xyRange[1], xyRange[2], xyRange[3]))

    FIG_AFTER(xyRange)

def toggle_selector(event):
    print(' Key pressed.')
    if event.key in ['Q', 'q'] and toggle_selector.RS.active:
        print(' RectangleSelector deactivated.')
        toggle_selector.RS.set_active(False)
    if event.key in ['A', 'a'] and not toggle_selector.RS.active:
        print(' RectangleSelector activated.')
        toggle_selector.RS.set_active(True)

def FIG_AFTER(xyRange):

    rcParams['font.size'] = 10
    inchY = 6.0
    inchX = (inchY * (xyRange[2]-xyRange[0])/(xyRange[3]-xyRange[1]))*5.0

    fig = plt.figure(figsize=(inchX, inchY))
    pixelY = fig.dpi * inchY

    ax1 = fig.add_subplot(141)
    ax2 = fig.add_subplot(142)
    ax3 = fig.add_subplot(143)
    ax4 = fig.add_subplot(144)

    fname = '/Users/hoch4240/Chen/work/03_OCO2/pre/data/20151206/nasa-worldview-2015-12-06.tiff'
    xx, yy, RGB = READ_GEOTIFF(fname, xyRange=xyRange, ravel=False)
    fname  = '/Users/hoch4240/Chen/work/03_OCO2/pre/data/20151206/OCO2/oco2_LtCO2_151206_B7305Br_160712000329s.nc4'
    f_oco2 = RDATA_OCO2(fname, xyRange=xyRange)

    ax1.set_xlim((xyRange[0], xyRange[2]))
    ax1.set_ylim((xyRange[1], xyRange[3]))
    extent = [xyRange[0], xyRange[2], xyRange[1], xyRange[3]]
    ax1.imshow(np.swapaxes(RGB, 0, 1), extent=extent, aspect='auto')
    cs = ax1.scatter(f_oco2.lon, f_oco2.lat, c=f_oco2.xco2, cmap='jet', edgecolor='none', marker='s')
    fig.colorbar(cs, ax=ax1, orientation='vertical')
    ax1.set_xlabel('Longitude')
    ax1.set_ylabel('Latitude')
    ax1.set_title('NASA Worldview (OCO2 Overlay)')

    Nbin     = 30
    binEdges = np.linspace(xyRange[1], xyRange[3], Nbin+1)
    xco2_mean = np.zeros(Nbin)
    xco2_std  = np.zeros(Nbin)
    lat_mean  = np.zeros(Nbin)
    for i in range(Nbin):
        logic = (f_oco2.lat>=binEdges[i])&(f_oco2.lat<binEdges[i+1])
        xco2_mean[i] = f_oco2.xco2[logic].mean()
        xco2_std[i]  = f_oco2.xco2[logic].std()
        lat_mean[i]  = f_oco2.lat[logic].mean()

    # ax2.set_xlim((xyRange[0], xyRange[2]))
    ax2.plot(xco2_mean, lat_mean)
    ax2.fill_betweenx(lat_mean, xco2_mean-xco2_std, xco2_mean+xco2_std, facecolor='gray', alpha=0.4)
    ax2.set_ylim((xyRange[1], xyRange[3]))
    ax2.set_ylabel('Latitude')
    ax2.set_title('Mixing Ratio of $\mathbf{\mathrm{CO_2}}$')

    ax3.set_xlim((xyRange[0], xyRange[2]))
    ax3.set_ylim((xyRange[1], xyRange[3]))
    ax3.set_title('Plot 2')

    ax4.set_xlim((xyRange[0], xyRange[2]))
    ax4.set_ylim((xyRange[1], xyRange[3]))
    ax4.set_title('Plot 3')

    plt.tight_layout(pad=0.4, w_pad=1.5, h_pad=1.0)
    plt.show()

def TEST(fname):

    """
    https://gibs.earthdata.nasa.gov/image-download?TIME=2017125&extent=60.24205784871318,41.39386867015344,75.87059788777569,55.91596339671594&epsg=4326&layers=MODIS_Terra_CorrectedReflectance_TrueColor,Coastlines&opacities=1,1&worldfile=false&format=image/geotiff&width=7114&height=6610
    https://gibs.earthdata.nasa.gov/image-download?TIME=2017125&extent=60.24205784871318,41.39386867015344,75.87059788777569,55.91596339671594&epsg=4326&layers=MODIS_Aqua_CorrectedReflectance_TrueColor,Coastlines&opacities=1,1&worldfile=false&format=image/geotiff&width=7114&height=6610
    https://gibs.earthdata.nasa.gov/image-download?TIME=2015339&extent=-216.0433279539029,-30.006667543161655,-213.8960278806059,-23.95247428094938&epsg=4326&layers=MODIS_Aqua_CorrectedReflectance_TrueColor,Coastlines,Calipso_Orbit_Asc&opacities=1,1,1&worldfile=false&format=image/geotiff&width=489&height=1378
    """
    xyRange=[144.0, -30.0, 146.0, -24.0]
    xx, yy, RGB = READ_GEOTIFF(fname, xyRange=xyRange, ravel=False)

    # figure settings
    fig = plt.figure(figsize=(8, 6))
    ax1 = fig.add_subplot(111, aspect='equal')
    ax1.set_title('Pleaes drag-select a region you are interested in ...', fontsize=16)
    extent = [xyRange[0], xyRange[2], xyRange[1], xyRange[3]]
    ax1.imshow(np.swapaxes(RGB, 0, 1), extent=extent)
    ax1.axis('off')

    toggle_selector.RS = RectangleSelector(ax1, line_select_callback,
                                           drawtype='box', useblit=True,
                                           button=[1, 3],  # don't use middle button
                                           minspanx=5, minspany=5,
                                           spancoords='pixels',
                                           interactive=True)


    plt.connect('key_press_event', toggle_selector)
    plt.show()

if __name__ == '__main__':

    import matplotlib as mpl
    import matplotlib.pyplot as plt
    from matplotlib.ticker import FixedLocator
    from matplotlib.widgets import RectangleSelector
    from matplotlib import rcParams
    from pre_SHARE import READ_GEOTIFF
    # date     = datetime.datetime(2015, 12, 6)
    # init = MAIN_INIT(date)
    fname = '/Users/hoch4240/Chen/work/03_OCO2/pre/data/20151206/nasa-worldview-2015-12-06.tiff'
    TEST(fname)
