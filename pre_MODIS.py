#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import glob
import ftplib
import datetime
from cfg import *

def GDATA_MODIS(dtime_s, dtime_e, tag, fdirOut=os.getcwd(), verbose=True):

    if dtime_s.day != dtime_e.day:
        exit('Error   [GDATA_MODIS]: not support different days.')

    tag0 = tag.split('/')[2]
    if not os.path.isdir(fdirOut):
        os.system('mkdir -p %s' % fdirOut)

    ftpSite  = 'ladsftp.nascom.nasa.gov'
    try:
        ftpMODIS = ftplib.FTP(ftpSite)
        ftpMODIS.login()
    except ftplib.all_errors:
        exit('Error    [GDATA_MODIS]: cannot ftp to %s.' % ftpSite)

    doy      = (dtime_s-datetime.datetime(dtime_s.year-1, 12, 31)).days

    ftpFdir  = '%s/%4.4d/%3.3d' % (tag, dtime_s.year, doy)
    try:
        ftpMODIS.cwd(ftpFdir)
    except ftplib.all_errors:
        exit('Error    [GDATA_MODIS]: data is not available for the requested date.')

    fnames = ftpMODIS.nlst()
    NFile = len(fnames)

    if NFile > 0:
        print('Message [GDATA_MODIS]: start download for %s ...' % tag0)
        for fname in fnames:
            if os.path.exists('%s/%s' % (fdirOut, fname)):
                print('Warning [GDATA_MODIS]: %s exists under %s.' % (fname, fdirOut))
            else:
                strings  = fname.split('.')
                dtimeStr = strings[1]+strings[2]
                dtime0   = datetime.datetime.strptime(dtimeStr, 'A%Y%j%H%M')
                if dtime_s-datetime.timedelta(minutes=10.0) <= dtime0 <= dtime_e+datetime.timedelta(minutes=10.0):
                    ftpMODIS.retrbinary('RETR %s' % fname, open('%s/%s' % (fdirOut, fname), 'wb').write)
                    if verbose and glob.glob('%s/%s' % (fdirOut, fname)):
                        print('Message [GDATA_MODIS]: %s has been downloaded under %s.' % (fname, fdirOut))
    else:
        exit('Error    [GDATA_MODIS]: data is not available for the requested date.')

    ftpMODIS.quit()

    NFile_check = len(glob.glob('%s/%s*.hdf' % (fdirOut, tag0)))
    if NFile_check == NFile:
        print('Message [GDATA_MODIS]: complete download for %s ...' % tag0)
    else:
        print('Warning [GDATA_MODIS]: incomplete download for %s, please check.' % tag0)

if __name__ == '__main__':

    message = 'This program is an example to show how to grab Terra MODIS cloud L2 data for a day specified by user.'
    print(message)

    Nargv = len(sys.argv)
    if Nargv == 1:
        print('Please enter a date in the format of yyyy-mm-dd')
        dateStr = input(':')
    elif Nargv == 2:
        dateStr = sys.argv[1]
    else:
        exit('Error [pre_MODIS]: too many arguments.')

    try:
        date = datetime.datetime.strptime(dateStr, '%Y-%m-%d')
    except ValueError:
        exit('Error [pre_MODIS]: can not read in the entered date. The correct format is yyyy-mm-dd.')

    GDATA_MODIS(date)
